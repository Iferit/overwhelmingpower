﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
	public int hits=1;
	public int grabs=1;
	public int moves=1;
	public int swings=1;
	public TileControl currentTile;

	private int _hits;
	private int _grabs;
	private int _moves;
	private int _swings;
	private GameObject[] adjasentObjects;

	// Update is called once per frame
	void Update()
	{

	}

	public void RoundReset()
	{
		_grabs = grabs;
		_moves = moves;
		_swings = swings;
	}

	private void GetAjasent()
	{

	}

	public void Attack()
	{
		GameObject target;

		target = this.gameObject;
		//find a target
		Attack(target);
	}

	public void Attack(GameObject tar)
	{
		var attackTarget = tar.GetComponent<Unit>() as Unit;

		if (attackTarget != null)
		{
			attackTarget.Hit();
		}
		swings--;
	}

	public void Hit()
	{
		_hits--;
		if (_hits <= 0)
		{
			Die();
		}
	}

	private void Die()
	{
		Destroy(this.gameObject, .1f);
	}

	public void Gather()
	{

	}

	public void Move(TileControl dest)
	{
		if (dest.isOccupied == false)
		{
			//move to attach point
			//reduce moves
			//set tile to refrence this unit
			//set tile isOccupied to true
		}
		else
		{
			//check faction, if not same as this, move
		}
	}
}
