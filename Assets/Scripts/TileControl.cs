﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileControl : MonoBehaviour {
	[Header("Adjacent Tiles")]
	public TileControl northWestTile;
	public TileControl northEastTile;
	public TileControl westTile;
	public TileControl eastTile;
	public TileControl southWestTile;
	public TileControl southEastTile;
	public LayerMask tileLayer;

	[Header("Refrence")]
	public Transform p1AttachmentPoint;
	public Transform p2AttachmentPoint;
	public GameObject p1StationedUnit;
	public GameObject p2StationedUnit;

	float XOffsetHorizontal = 1f; //1.8f offset
	float XOffsetDiaginal = .5f; //.9f offset
	float YOffsetDiaginal = .9f; //1.55f offset
	float checkPossitionTalerance = .1f;

	

	[Header("Current Unit")]
	public bool isOccupied = false;

	private void Start()
	{
		FindAdjasent();
	}

	public void setTransformPos(Vector3 newPos)
	{
		transform.position = newPos;
	}

	public void AlignAdjasent()
	{

	}

	public void FindAdjasent()
	{
		Collider[] adjacentObjects;

		adjacentObjects = Physics.OverlapSphere(transform.position, 2f,tileLayer);

		List<GameObject> adjasentTiles;
		adjasentTiles = new List<GameObject>();

		foreach (Collider col in adjacentObjects)
		{
			if (col.gameObject.tag == "FloorTile" && col.gameObject != this.gameObject)
			{
				adjasentTiles.Add(col.gameObject);
			}
		}

		foreach(GameObject obj in adjasentTiles)
		{
			var heading = obj.transform.position - transform.position;
			var distence = heading.magnitude;
			var direction = heading / (heading.magnitude);

			//Debug.Log("from " + gameObject.name + " to " + obj.gameObject.name + ": header= " + heading.ToString() + " distence = " + distence.ToString() + " direction = " + direction.ToString());

			MapTileDirection(obj, direction);
		}

	}

	private void MapTileDirection(GameObject tile, Vector3 dir)
	{
		if(dir.x > XOffsetDiaginal - checkPossitionTalerance)
		{
			//check north east
			if(dir.z>YOffsetDiaginal-checkPossitionTalerance && northEastTile == null)
			{
				northEastTile = tile.GetComponent<TileControl>();
				return;
			}

			//check south east
			if (dir.z < (-YOffsetDiaginal) + checkPossitionTalerance && southEastTile == null)
			{
				southEastTile = tile.GetComponent<TileControl>();
				return;
			}

			//check east
			if (dir.z > (-YOffsetDiaginal) - checkPossitionTalerance && dir.z < YOffsetDiaginal + checkPossitionTalerance && eastTile == null)
			{
				eastTile = tile.GetComponent<TileControl>();
				return;
			}
		}

		if (dir.x < (-XOffsetDiaginal) + checkPossitionTalerance)
		{
			//check north east
			if (dir.z > YOffsetDiaginal - checkPossitionTalerance && northWestTile == null)
			{
				northWestTile = tile.GetComponent<TileControl>();
				return;
			}

			//check south east
			if (dir.z < (-YOffsetDiaginal) + checkPossitionTalerance && southWestTile == null)
			{
				southWestTile = tile.GetComponent<TileControl>();
				return;
			}

			//check east
			if (dir.z > (-YOffsetDiaginal) - checkPossitionTalerance && dir.y < YOffsetDiaginal + checkPossitionTalerance && westTile == null)
			{
				westTile = tile.GetComponent<TileControl>();
				return;
			}
		}
	}

}
